/*
 Andrew McConville
 andrew@andrewmcconville.com
*/

jQuery(document).ready(function($){

	//check for empty required field
	$('input[required]').on('blur', function(){
		if($(this).val() == ''){
			$(this).siblings('.alert-success').hide();
			$(this).siblings('.alert-danger').fadeIn();
		} else {
			$(this).siblings('.alert-danger').hide();
			$(this).siblings('.alert-success').fadeIn();
		}
	});

	$('form').on('blur', 'input:not([required]), select', function(){
		$(this).siblings('.alert-success').fadeIn();
	});

	//hide empty input alert on focus
	$('form').on('focus', 'input', function(){
		$(this).siblings('.alert-danger').fadeOut();
		$(this).siblings('.alert-success').fadeOut();
	});

	//mark primary phone number
	$('#phone-numbers-wrapper').on('click', '.primary', function(){
		$('#phone-numbers-wrapper .radio-text').removeClass('active-radio');
		$('.radio-text', this).addClass('active-radio');
		$('input', this).prop('checked', true);
	});

	//mark primary email
	$('#emails-wrapper').on('click', '.primary', function(){
		$('#emails-wrapper .radio-text').removeClass('active-radio');
		$('.radio-text', this).addClass('active-radio');
		$('input', this).prop('checked', true);
	});

	//clone phone
	$('#clone-phone').on('click', function(){
		var phoneView = [
		                '<div class="input-group">',
		                  '<div class="combined-group-heading primary">',
		                    '<input type="radio" name="phone" value="primary">',
		                    '<span class="combined-group-heading-text radio-text">Primary</span>',
		                  '</div>',
		                  '<div class="combined-group">',
		                    '<div class="form-group">',
		                      '<input class="form-control" type="text" id="phone" name="phone" placeholder="123-456-7890">',
		                      '<label for="phone">Phone Number</label>',
		                      '<div class="alert alert-success">',
		                        '<span class="glyphicon glyphicon-ok"></span>',
		                      '</div>',
		                    '</div>',
		                    '<div class="form-group">',
		                      '<input class="form-control" type="text" id="extension" name="extension" placeholder="1234">',
		                      '<label for="extension">Extension</label>',
		                      '<div class="alert alert-success">',
		                        '<span class="glyphicon glyphicon-ok"></span>',
		                      '</div>',
		                    '</div>',
		                    '<div class="form-group">',
		                      '<select class="form-control">',
		                      	'<option value="Work">Work</option>',
		                      	'<option value="Cell">Cell</option>',
		                      	'<option value="Home">Home</option>',
		                      '</select>',
		                      '<label>Location Type</label>',
		                      '<div class="alert alert-success">',
		                        '<span class="glyphicon glyphicon-ok"></span>',
		                      '</div>',
		                    '</div>',
		                  '</div>',
		                '</div>'
			            ].join('');

		$('#phone-numbers-wrapper').append(phoneView);
	});

	//clone email
	$('#clone-email').on('click', function(){
		var emailView = [
		                '<div class="input-group">',
		                  '<div class="combined-group-heading primary">',
		                    '<input type="radio" name="email" value="primary">',
		                    '<span class="combined-group-heading-text radio-text">Primary</span>',
		                  '</div>',
		                  '<div class="combined-group">',
		                    '<div class="form-group">',
		                      '<input class="form-control" type="text" id="email" name="email" placeholder="andrew@andrewmcconville.com">',
		                      '<label for="email">Email</label>',
		                      '<div class="alert alert-success">',
		                        '<span class="glyphicon glyphicon-ok"></span>',
		                      '</div>',
		                    '</div>',
		                  '</div>',
		                '</div>'
			            ].join('');

		$('#emails-wrapper').append(emailView);
	});

	//clone communication
	$('#clone-communication').on('click', function(){
		var emailView = [
						'<div class="form-group-double clearfix">',
						  '<div class="form-group form-group-half">',
						    '<input class="form-control" type="date" name="bday">',
						    '<label>Date</label>',
						  '</div>',
						  '<div class="form-group form-group-half">',
						    '<select class="form-control">',
						        '<option value="General">Phone</option>',
						        '<option value="Agent">In Person</option>',
						        '<option value="Claims Officer">Mail</option>',
						    '</select>',
						    '<label>Method</label>',
						  '</div>',
						  '<textarea class="form-control" placeholder="Comments"></textarea>',
						'</div>'
			            ].join('');

		$('#communication-history-wrapper').append(emailView);
	});

	//clone notes
	$('#clone-notes').on('click', function(){
		var emailView = [
						'<div class="form-group-double">',
						'<div class="form-group form-group-top">',
						  '<input class="form-control" type="text" name="title" placeholder="Title">',
						  '<label>Title</label>',
						'</div>',
						'<div class="form-group-middle clearfix">',
						  '<div class="form-group form-group-half">',
						    '<input class="form-control" type="date" name="date">',
						    '<label>Date</label>',
						  '</div>',
						  '<div class="form-group form-group-half">',
						    '<input class="form-control" type="text" name="creator" placeholder="Name">',
						    '<label>Created by</label>',
						  '</div>',
						'</div>',
						'<textarea class="form-control" placeholder="Description"></textarea>',
						'</div>'
			            ].join('');

		$('#notes-wrapper').append(emailView);
	});



	/*
	 * Stubs
	 */
	$('.photo-upload').on('click', function(){
		alert('Open file browser or device camera.');
	});

});//end jQuery